KNOCKS_PATH=knocks
TEST_PATH=tests
test-all: test-modules test
test: lint py-test
py-test:
	PYTHONPATH=. pipenv run pytest $(TEST_PATH)
lint: isort
	pipenv run flake8 $(KNOCKS_PATH) $(TEST_PATH)
isort:
	PYTHONPATH=. pipenv run isort $(KNOCKS_PATH)/*.py $(TEST_PATH)/*.py
test-modules:
	make -C modules/ test
