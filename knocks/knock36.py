from collections import defaultdict
from itertools import chain
from typing import Dict, List

from pymecab.morph import Morph
from pymecab.parser import parse


def _parse_neko_mecab() -> List[List[Morph]]:
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def _count_morphs(
    morphs: List[Morph]
) -> Dict[Morph, int]:
    d = defaultdict(int)
    for m in morphs:
        d[m] += 1
    return {
        key: value
        for key, value
        in sorted(d.items(), key=lambda x: -x[1])
    }


def _calculate_morphs_frequency_of_appearance(
        morphs_count: Dict[Morph, int]
) -> Dict[Morph, float]:
    all_counts = sum(morphs_count.values())
    return {
        key: value / all_counts
        for key, value
        in morphs_count.items()
    }


def count_morphs_frequency_of_appearance_in_neko() -> Dict[Morph, float]:
    flatten_neko = list(chain.from_iterable(_parse_neko_mecab()))
    morphs = _count_morphs(flatten_neko)
    frequencies = _calculate_morphs_frequency_of_appearance(morphs)
    return frequencies
