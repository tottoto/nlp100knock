import argparse
from datetime import datetime as dt
from typing import List


def parse_lines_to_tsv(lines: List[str]) -> List[List[str]]:
    return [line.strip().split() for line in lines]


def sort_tsv(tsv: List[List[str]]) -> List[List[str]]:
    return sorted(
        tsv,
        key=lambda x: (
            float(x[2]),
            dt.strptime(x[3], '%Y-%m-%d')
        ),
        reverse=True
    )


def sort_file(path: str) -> None:
    with open(path, 'r') as f:
        lines = f.readlines()
    tsv = parse_lines_to_tsv(lines)
    sorted_tsv = sort_tsv(tsv)
    return '\n'.join(['\t'.join(lines) for lines in sorted_tsv]) + '\n'


def get_args():
    parser = argparse.ArgumentParser(
        description='Sort hightemp file.'
    )
    parser.add_argument(
        'file', type=str,
        help='Path to target file'
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    print(sort_file(args.file), end='')
