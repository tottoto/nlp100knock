import json
import os
import re
import urllib.request
from typing import Dict, List


def _extract_basic_information(s: str) -> str:
    pattern = r'\{\{基礎情報.*?$(.*?)^\}\}$'
    content = re.findall(pattern, s, flags=re.DOTALL | re.MULTILINE)
    return content[0]


def _parse_basic_information(s: str) -> List[Dict[str, str]]:
    pattern = r'^\|([^\s]+?)\s*=\s*(.+?)(?:(?=\n\|)|(?=\n$))'
    parsed = re.findall(pattern, s, flags=re.MULTILINE | re.DOTALL)
    return {key: value for key, value in parsed}


def _create_url_to_get_mediawiki_file_url(file_name: str) -> str:
    encoded_file_name = urllib.parse.quote(file_name)
    scheme = 'https'
    netloc = 'www.mediawiki.org'
    path = os.path.join('w', 'api.php')
    params = ''
    query = '&'.join([
        'action=query',
        f'titles=File:{encoded_file_name}',
        'format=json',
        'prop=imageinfo',
        'iiprop=url'
    ])
    fragment = ''
    return urllib.parse.urlunparse((
        scheme, netloc, path, params, query, fragment
    ))


def _get_content_from_url(url: str) -> dict:
    request = urllib.request.Request(url)
    connection = urllib.request.urlopen(request)
    content = json.loads(connection.read().decode())
    connection.close()
    return content


def _parse_image_url_from_response(content: dict) -> str:
    return content['query']['pages']['-1']['imageinfo'][0]['url']


def get_flag_url_from_england() -> str:
    with open('./data/england.json') as f:
        england = json.load(f)['text']
    basic_information = _extract_basic_information(england)
    parsed_information = _parse_basic_information(basic_information)
    file_name = parsed_information['国旗画像']
    request_url = _create_url_to_get_mediawiki_file_url(file_name)
    response = _get_content_from_url(request_url)
    image_url = _parse_image_url_from_response(response)
    return image_url
