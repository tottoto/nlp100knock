import argparse
import subprocess


def get_args():
    parser = argparse.ArgumentParser(
        description='Extract first lines of the file.'
    )
    parser.add_argument(
        'path', type=str,
        help='Path to the file.'
    )
    parser.add_argument(
        '--number', '-n', type=int, required=True,
        help='An integer for lines number.'
    )
    return parser.parse_args()


def head(path: str, n: int) -> str:
    with open(path, 'r') as f:
        headlines = [line for i, line in enumerate(f) if i < n]
    return ''.join(headlines)


def head_by_command(path: str, n: int) -> str:
    return subprocess.run(
        ['head', '-n', str(n), path],
        stdout=subprocess.PIPE
    ).stdout.decode()


if __name__ == '__main__':
    args = get_args()
    headlines = head(path=args.path, n=args.number)
    print(headlines, end='')
