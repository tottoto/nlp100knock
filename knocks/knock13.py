import subprocess
from typing import List


def merge_cols_as_tsv(paths: List[str]) -> str:
    cols = [open(path).read() for path in paths]
    return '\n'.join(map('\t'.join, zip(*map(str.split, cols)))) + '\n'


def merge_cols_as_tsv_by_paste(paths: List[str]) -> str:
    res = subprocess.run(['paste', *paths], stdout=subprocess.PIPE)
    return res.stdout.decode()
