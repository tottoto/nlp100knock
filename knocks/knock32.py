from itertools import chain

from pymecab.parser import parse


def _parse_neko_mecab():
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def extract_base_of_verb_from_neko():
    neko = _parse_neko_mecab()
    return set(w.base for w in chain.from_iterable(neko) if w.pos == '動詞')
