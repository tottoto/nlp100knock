import json
import re
from typing import List, Tuple


def collect_sections() -> List[Tuple[int, List[str]]]:
    with open('./data/england.json', 'r') as f:
        england_text = json.load(f)['text']
    sections = []
    for level in range(1, 10):
        regexp_format = fr'^={{{level + 1}}}([^=\s]+)={{{level + 1}}}$'
        regexp = re.compile(regexp_format, flags=re.MULTILINE)
        section = regexp.findall(england_text)
        if section:
            sections.append((level, section))
    return sections
