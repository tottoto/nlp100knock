import json
import re


def _remove_emphasis_markup(s: str) -> str:
    for n in [2, 3, 5]:
        pattern = fr'(?<!\')\'{{{n}}}(?!\')([^\']+)(?<!\')\'{{{n}}}(?!\')'
        regexp = re.compile(pattern, re.MULTILINE)
        s = regexp.sub('\\1', s)
    return s


def _remove_internal_link_markup(s: str) -> str:
    pattern = r'\[\[(?!Category|ファイル)([^\[\]]+?)(?:\|[^\[\]]*)?\]\]'
    regexp = re.compile(pattern)
    return regexp.sub('\\1', s)


def _remove_markup(s: str) -> str:
    s = _remove_emphasis_markup(s)
    s = _remove_internal_link_markup(s)
    return s


def remove_emphasis_and_internal_link_markup_from_england() -> str:
    with open('./tests/data/england.json') as f:
        england = json.load(f)['text']
    return _remove_markup(england)
