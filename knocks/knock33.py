from itertools import chain

from pymecab.parser import parse


def _parse_neko_mecab():
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def extract_morph_of_sahen_connection_noun_from_neko():
    parsed = _parse_neko_mecab()
    return [w for w in chain.from_iterable(parsed)
            if 'サ変接続' in [w.pos1, w.pos2, w.pos3]]
