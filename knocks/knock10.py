import subprocess


def count_file_lines(path: str) -> int:
    with open(path, 'r') as f:
        s = f.read()
    return s.count('\n')


def count_file_lines_by_wc(path: str) -> int:
    with open(path, 'r') as f:
        res = subprocess.run(['wc', '-l'], stdin=f, stdout=subprocess.PIPE)
    return int(res.stdout.decode().strip())
