from itertools import chain


def add_patoka_and_takusi() -> str:
    s1 = 'パトカー'
    s2 = 'タクシー'
    return ''.join(chain.from_iterable(zip(s1, s2)))
