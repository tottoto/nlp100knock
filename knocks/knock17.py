import argparse


def extract_first_column_of_file(
        input_path: str,
) -> str:
    with open(input_path, 'r') as f:
        lines = f.readlines()
    first_column = {line.strip().split()[0] for line in lines}
    return '\n'.join(sorted(first_column)) + '\n'


def get_args():
    parser = argparse.ArgumentParser(
        description='Get first column string set of tsv file.'
    )
    parser.add_argument(
        'input', type=str,
        help='Path to tsv file.'
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    print(extract_first_column_of_file(args.input), end='')
