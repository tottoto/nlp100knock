import argparse
import os
from typing import List


class SuffixIndex:

    def __init__(self, length: int, decimal: int):
        self._length = length
        self._decimal = decimal
        self._index = [0 for i in range(length)]
        self._i = 0

    def __getitem__(self, position):
        return self._index[position]

    def __next__(self):
        index_ = list(reversed(self._index))
        try:
            if index_[self._i] >= self._decimal - 1:
                self._i += 1
        except IndexError:
            raise StopIteration
        if self._i >= self._length:
            raise StopIteration
        index_[self._i] += 1
        self._index = list(reversed(index_))
        return self


class Suffix:

    ALPHABET = 'abcdefghijklmnopqrstuvwxyz'

    def __init__(self, length: int):
        self._index = SuffixIndex(length, len(self.ALPHABET))
        self._suffix = self._get_suffix()

    def _get_suffix(self) -> str:
        self._suffix = ''.join(self.ALPHABET[i] for i in self._index)
        return self._suffix

    def __iter__(self):
        return self

    def __next__(self):
        suffix = self._get_suffix()
        self._index = next(self._index)
        return suffix

    def __str__(self):
        return self._suffix


def split_lines(
        target: List[str],
        line_count: int
) -> List[List[str]]:
    chunk = - (- len(target) // line_count)  # round up
    return [target[line_count*i:line_count*(i+1)] for i in range(chunk)]


def split_file(
        path: str,
        prefix: str,
        suffix_length: int,
        line_count: int
) -> List[str]:
    with open(path, 'r') as f:
        lines = f.readlines()
    grouped_lines = split_lines(lines, line_count)
    suffixes = Suffix(suffix_length)
    for suffix, group in zip(suffixes, grouped_lines):
        out_path = os.path.join(f'{prefix}{suffix}')
        with open(out_path, 'w') as f:
            f.writelines(group)


def get_args():
    parser = argparse.ArgumentParser(
        description='Split a file into pieces.'
    )
    parser.add_argument(
        '--suffix_length', '-a', type=int, default=2,
        help='Suffix_length letters of output file name.'
    )
    parser.add_argument(
        '--line_count', '-n', type=int, required=True,
        help='Create smaller files n lines in length.'
    )
    parser.add_argument(
        'file', type=str,
        help='Target file path.'
    )
    parser.add_argument(
        'name', default='', type=str,
        help='Prefix for output file name.'
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    split_file(
        path=args.file,
        prefix=args.name,
        suffix_length=args.suffix_length,
        line_count=args.line_count
    )
