import json
from typing import List


def _load_from_one_line_json(path: str) -> List[dict]:
    with open(path, 'r') as f:
        lines = f.readlines()
    return [json.loads(l) for l in lines]


def _find_record_by_title(records: dict, title: str) -> dict:
    return [record for record in records if record['title'] == title][0]


def load_england_record(path: str) -> dict:
    wiki_data = _load_from_one_line_json('./data/jawiki-country.json')
    england = _find_record_by_title(wiki_data, title='イギリス')
    return england
