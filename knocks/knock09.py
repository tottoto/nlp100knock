import random


def _typoglycemia_word(w: str) -> str:
    if len(w) <= 4:
        return w
    return w[0] + ''.join(random.sample(w[1:-1], len(w) - 2)) + w[-1]


def typoglycemia(text: str) -> str:
    return ' '.join([_typoglycemia_word(w) for w in text.split()])
