import json
import re


def collect_category_lines():
    with open('./data/england.json', 'r') as f:
        england = json.load(f)
    return re.findall('.*Category.*', england['text'])
