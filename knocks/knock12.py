import subprocess
from typing import List, Tuple


def parse_tsv(s: str) -> List[List[str]]:
    lines = s.strip().split('\n')
    return [line.split('\t') for line in lines]


def transpose_tsv(tsv: List[List[str]]) -> List[Tuple[str]]:
    return list(zip(*tsv))


def get_cols(tsv: List[List[str]], cols: List[int]) -> List['str']:
    transposed = transpose_tsv(tsv)
    return [col for i, col in enumerate(transposed) if i in cols]


def save_col(col: List[str], path: str) -> None:
    s = '\n'.join(col) + '\n'
    with open(path, 'w') as f:
        f.write(s)


def save_col1_and_col2_of_hightemp(
        col1_path: str,
        col2_path: str
) -> None:
    with open('./data/hightemp.txt', 'r') as f:
        s = f.read()
    tsv = parse_tsv(s)
    col1, col2 = get_cols(tsv, cols=[0, 1])
    save_col(col1, col1_path)
    save_col(col2, col2_path)


def get_col_by_cut(s: str, index: int) -> str:
    echo = subprocess.Popen(
        ['echo', '-n', s],
        stdout=subprocess.PIPE,
        universal_newlines=True
    )
    res = subprocess.run(
        ['cut', '-f', str(index)],
        stdin=echo.stdout,
        stdout=subprocess.PIPE,
        universal_newlines=True
    )
    return res.stdout


if __name__ == '__main__':
    import os
    dir_path = './data'
    col1_file_name = 'col1.txt'
    col2_file_name = 'col2.txt'
    save_col1_and_col2_of_hightemp(
        col1_path=os.path.join(dir_path, col1_file_name),
        col2_path=os.path.join(dir_path, col2_file_name)
    )
