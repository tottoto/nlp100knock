from collections import defaultdict
from itertools import chain
from typing import Dict, List

from matplotlib import pyplot

from pymecab.morph import Morph
from pymecab.parser import parse


def _parse_neko_mecab() -> List[List[Morph]]:
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def _count_morphs(
    morphs: List[Morph]
) -> Dict[Morph, int]:
    d = defaultdict(int)
    for m in morphs:
        d[m] += 1
    return {
        key: value
        for key, value
        in sorted(d.items(), key=lambda x: -x[1])
    }


def plot_hist_words_of_neko(font: str = None) -> pyplot:
    if font is not None:
        import matplotlib
        matplotlib.rcParams['font.family'] = font
    neko = _parse_neko_mecab()
    counts = _count_morphs(chain.from_iterable(neko))
    pyplot.hist(
        list(counts.values()),
        bins=20,
        range=(1, 20)
    )
    pyplot.xlim(left=1, right=20)
    return pyplot


if __name__ == '__main__':
    # Move to this project root and execute a follow command.
    # `PYTHONPATH=. pipenv run ./knocks/knock38.py`
    # You will get a result image as `results/knocks38.png`
    p = plot_hist_words_of_neko(font='Ricty')
    p.savefig('./results/knock38.png')
