from collections import defaultdict
from itertools import chain
from typing import Dict, List

from matplotlib import pyplot

from pymecab.morph import Morph
from pymecab.parser import parse


def _parse_neko_mecab() -> List[List[Morph]]:
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def _count_morphs(
    morphs: List[Morph]
) -> Dict[Morph, int]:
    d = defaultdict(int)
    for m in morphs:
        d[m] += 1
    return {
        key: value
        for key, value
        in sorted(d.items(), key=lambda x: -x[1])
    }


def plot_zipf_of_neko() -> pyplot.Figure:
    neko = _parse_neko_mecab()
    counts = list(_count_morphs(chain.from_iterable(neko)).values())
    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.scatter(range(1, len(counts) + 1), counts)
    return fig


if __name__ == '__main__':
    # Move to this project root and execute a follow command.
    # `PYTHONPATH=. pipenv run ./knocks/knock39.py`
    # You will get a result image as `results/knocks39.png`
    fig = plot_zipf_of_neko()
    fig.savefig('./results/knock39.png')
