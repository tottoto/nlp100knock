from pymecab.parser import parse


def parse_neko_mecab():
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)
