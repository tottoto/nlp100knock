def _is_small_letter(c):
    # ord('a') = 97
    # ord('z') = 122
    return 97 <= ord(c) <= 122


def _cipher_letter(c):
    return chr(219 - ord(c)) if _is_small_letter(c) else c


def cipher(s: str) -> str:
    return ''.join([_cipher_letter(c) for c in s])
