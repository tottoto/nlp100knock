import subprocess


def tail(path: str, n: int) -> str:
    with open(path, 'r') as f:
        lines = f.readlines()
    return ''.join(lines[-n:])


def tail_by_command(path: str, n: int) -> str:
    return subprocess.run(
        ['tail', '-n', str(n), path],
        stdout=subprocess.PIPE
    ).stdout.decode()
