import argparse
from collections import defaultdict
from typing import Dict, List, Tuple


def parse_lines_to_tsv(lines: List[str]) -> List[List[str]]:
    return [line.strip().split() for line in lines]


def count_first_columns_keyword(tsv: List[List[str]]) -> Dict[str, int]:
    d = defaultdict(int)
    for key in list(zip(*tsv))[0]:
        d[key] += 1
    return dict(d)


def sort_dict_by_value(d: Dict[str, int]) -> Tuple['str', int]:
    return {
        k: v for k, v
        in sorted(d.items(), key=lambda x: (x[1], x[0]), reverse=True)
    }


def collect_first_column_and_sort_by_counts(path: str) -> str:
    with open(path, 'r') as f:
        lines = f.readlines()
    tsv = parse_lines_to_tsv(lines)
    counts = count_first_columns_keyword(tsv)
    sorted_keywords = sort_dict_by_value(counts)
    return '\n'.join(sorted_keywords.keys()) + '\n'


def get_args():
    parser = argparse.ArgumentParser(
        description='Collect first column and sort by counts from tsv file.'
    )
    parser.add_argument(
        'path', type=str,
        help='Path to tsv file.'
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    res = collect_first_column_and_sort_by_counts(args.path)
    print(res, end='')
