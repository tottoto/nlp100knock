class SetCalculator:

    def __init__(self, text_1: str, text_2: str):
        self._set_1 = set(text_1)
        self._set_2 = set(text_2)

    def union(self):
        return self._set_1 | self._set_2

    def difference(self):
        return self._set_1 - self._set_2

    def intersection(self):
        return self._set_1 & self._set_2
