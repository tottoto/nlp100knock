import json
import re


def _remove_emphasis_markup(s: str) -> str:
    for n in [2, 3, 5]:
        pattern = fr'(?<!\')\'{{{n}}}(?!\')([^\']+)(?<!\')\'{{{n}}}(?!\')'
        regexp = re.compile(pattern, re.MULTILINE)
        s = regexp.sub('\\1', s)
    return s


def remove_emphasis_markup_from_england() -> str:
    with open('./data/england.json', 'r') as f:
        england_text = json.load(f)['text']
    return _remove_emphasis_markup(england_text)
