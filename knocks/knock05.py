from typing import List


def _ngram(seq: str, n: int) -> List[str]:
    return [''.join(chars) for chars
            in zip(*[seq[i:] for i in range(2)])]


def char_ngram(s: str, n: int) -> List[str]:
    return _ngram(seq=s, n=n)


def word_ngram(s: str, n: int) -> List[str]:
    return _ngram(seq=s.split(), n=n)
