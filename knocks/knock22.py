import json
import re


def collect_categories():
    with open('./data/england.json', 'r') as f:
        england = json.load(f)
    return re.findall(r'\[\[Category:(.*?)(?:\|.*)?\]\]', england['text'])
