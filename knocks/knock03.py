def _remove_punctuations(s: str) -> str:
    return s.replace('.', '').replace(',', '')


def get_pi():
    s = ('Now I need a drink, alcoholic of course, '
         'after the heavy lectures involving quantum mechanics.')
    return [len(w) for w in _remove_punctuations(s).split()]
