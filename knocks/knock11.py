import subprocess


def replace_tab_to_space(s: str) -> str:
    return s.replace('\t', ' ')


def replace_tab_to_space_by_tr(s: str) -> str:
    echo = subprocess.Popen(
        ['echo', '-n', s], stdout=subprocess.PIPE, universal_newlines=True)
    res = subprocess.run(
        ['tr', '\t', ' '],
        stdin=echo.stdout,
        stdout=subprocess.PIPE,
        universal_newlines=True
    )
    return res.stdout
