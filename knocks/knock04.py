def _get_element(i: int, s: str) -> int:
    if i in [1, 5, 6, 7, 8, 9, 15, 16, 19]:
        return s[:1]
    else:
        return s[:2]


def get_elements():
    s = ('Hi He Lied Because Boron Could Not Oxidize Fluorine. '
         'New Nations Might Also Sign Peace Security Clause. Arthur King Can.')
    return {i: _get_element(i, w) for i, w in enumerate(s.split(), start=1)}
