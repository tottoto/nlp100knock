import json
import re
from typing import Dict, List


def _extract_basic_information(s: str) -> str:
    pattern = r'\{\{基礎情報.*?$(.*?)^\}\}$'
    content = re.findall(pattern, s, flags=re.DOTALL | re.MULTILINE)
    return content[0]


def _parse_basic_information(s: str) -> List[Dict[str, str]]:
    pattern = r'^\|([^\s]+?)\s*=\s*(.+?)(?:(?=\n\|)|(?=\n$))'
    parsed = re.findall(pattern, s, flags=re.MULTILINE | re.DOTALL)
    return {key: value for key, value in parsed}


def parse_basic_information_in_england() -> List[Dict[str, str]]:
    with open('./data/england.json', 'r') as f:
        england_text = json.load(f)['text']
    basic_information_text = _extract_basic_information(england_text)
    return _parse_basic_information(basic_information_text)
