from typing import List, Optional, Tuple

from pymecab.morph import Morph
from pymecab.parser import parse


def _parse_neko_mecab() -> dict:
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def _find_nouns_sequence_at_the_point(
        sentence: List[Morph], index: int
) -> Tuple[Optional[List[Morph]], int]:
    i = index
    if sentence[index].pos != '名詞':
        return None, i + 1
    while(i < len(sentence) - 2):
        if sentence[i+1].pos == '名詞':
            i += 1
        else:
            break
    if i - index > 0:
        return sentence[index:i+1], i + 1
    else:
        return None, i + 1


def _find_nouns_sequences_from_sentence(
        sentence: List[Morph]
) -> List[List[Morph]]:
    res = []
    i = 0
    while(i < len(sentence) - 2):
        nouns, i = _find_nouns_sequence_at_the_point(sentence, i)
        if nouns is not None:
            res.append(nouns)
    return res


def _find_nouns_sequences_from_sentences(
        sentences: List[Morph]
) -> List[List[Morph]]:
    nouns = []
    for sentence in sentences:
        nouns.extend(_find_nouns_sequences_from_sentence(sentence))
    return nouns


def collect_nouns_sequences_from_neko() -> List[List[Morph]]:
    neko = _parse_neko_mecab()
    return _find_nouns_sequences_from_sentences(neko)
