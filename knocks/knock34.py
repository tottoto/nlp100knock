from typing import List, Tuple

from pymecab.morph import Morph
from pymecab.parser import parse


def _parse_neko_mecab() -> dict:
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def _is_connection_no(word: Morph) -> bool:
    return word.surface == 'の' and word.pos1 == '連体化'


def _find_connected_nouns_from_sentence(
        sentence: List[Morph]
) -> List[Tuple[Morph, Morph]]:
    noun_pairs = []
    i = 1
    while(i < len(sentence) - 2):
        if _is_connection_no(sentence[i]):
            pre = sentence[i-1]
            post = sentence[i+1]
            if pre.pos == post.pos == '名詞':
                noun_pairs.append((pre, post))
            i += 2
            continue
        i += 1
    return noun_pairs


def _find_connected_nouns_from_sentences(
        sentences: List[List[Morph]]
) -> List[Tuple[Morph, Morph]]:
    res = []
    for sentence in sentences:
        res.extend(_find_connected_nouns_from_sentence(sentence))
    return res


def collect_connected_nouns_from_neko() -> List[Tuple[Morph, Morph]]:
    neko = _parse_neko_mecab()
    return _find_connected_nouns_from_sentences(neko)
