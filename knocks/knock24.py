import json
import re
from typing import List


def collect_files() -> List[str]:
    with open('./data/england.json', 'r') as f:
        england_text = json.load(f)['text']
    return re.findall(
        r'^\[\[File:(.*?)\|.*\]\]$',
        england_text,
        flags=re.MULTILINE
    )
