from itertools import chain

from pymecab.parser import parse


def _parse_neko_mecab():
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def extract_surface_of_verb_from_neko():
    parsed = _parse_neko_mecab()
    return [w.surface for w in chain.from_iterable(parsed) if w.pos == '動詞']
