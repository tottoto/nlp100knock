from collections import defaultdict
from itertools import chain
from typing import Dict, Iterator, List, Tuple, Union

from matplotlib import pyplot

from pymecab.morph import Morph
from pymecab.parser import parse


def _parse_neko_mecab() -> List[List[Morph]]:
    with open('./data/neko.txt.mecab', 'r') as f:
        neko = f.read()
    return parse(neko)


def _count_morphs(
    morphs: List[Morph]
) -> Dict[Morph, int]:
    d = defaultdict(int)
    for m in morphs:
        d[m] += 1
    return {
        key: value
        for key, value
        in sorted(d.items(), key=lambda x: -x[1])
    }


def _cut_dict(
        input: dict,
        limit: int
) -> dict:
    return {k: v for i, (k, v) in enumerate(input.items()) if i < limit}


def _format_counting(
    morphs: Dict[Morph, int]
) -> Iterator[Tuple[Union[Morph, int]]]:
    return zip(*morphs.items())


def get_top_10_words_of_neko() -> List[Tuple[Union[Morph, int]]]:
    neko = _parse_neko_mecab()
    counts = _count_morphs(chain.from_iterable(neko))
    return _cut_dict(counts, 10)


def plot_top_10_words_of_neko(font: str = None) -> pyplot:
    if font is not None:
        import matplotlib
        matplotlib.rcParams['font.family'] = font

    top10 = get_top_10_words_of_neko()
    morphs, values = list(_format_counting(top10))
    pyplot.bar([m.base for m in morphs], values)
    pyplot.xlabel('base of word')
    pyplot.ylabel('count')
    return pyplot


if __name__ == '__main__':
    # Move to this project root and execute a follow command.
    # `PYTHONPATH=. pipenv run ./knocks/knock37.py`
    # You will get a result image as `results/knocks37.png`
    p = plot_top_10_words_of_neko(font='Ricty')
    p.savefig('./results/knock37.png')
