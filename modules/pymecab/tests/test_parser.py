from pymecab.parser import parse


class TestParse:

    def test_return_appropriate_morphs(self):
        expected = [
            [
                {
                    'surface': '一',
                    'pos': '名詞',
                    'pos1': '数',
                    'pos2': '*',
                    'pos3': '*',
                    'ctype': '*',
                    'cform': '*',
                    'base': '一',
                    'reading': 'イチ',
                    'pron': 'イチ'
                }
            ],
            [
                {
                    'surface': '　',
                    'pos': '記号',
                    'pos1': '空白',
                    'pos2': '*',
                    'pos3': '*',
                    'ctype': '*',
                    'cform': '*',
                    'base': '　',
                    'reading': '　',
                    'pron': '　'
                },
                {
                    'surface': '吾輩',
                    'pos': '名詞',
                    'pos1': '代名詞',
                    'pos2': '一般',
                    'pos3': '*',
                    'ctype': '*',
                    'cform': '*',
                    'base': '吾輩',
                    'reading': 'ワガハイ',
                    'pron': 'ワガハイ'
                },
                {
                    'surface': 'ニャーニャー',
                    'pos': '名詞',
                    'pos1': '一般',
                    'pos2': '*',
                    'pos3': '*',
                    'ctype': '*',
                    'cform': '*',
                    'base': '*',
                    'reading': '*',
                    'pron': '*'
                },
            ]
        ]
        with open('./tests/data/sample.txt', 'r') as f:
            sample = f.read()
        actual = parse(sample)

        for i, expected_line in enumerate(expected):
            for j, expected_word in enumerate(expected_line):
                for key, expected_value in expected_word.items():
                    assert getattr(actual[i][j], key) == expected_value
