from distutils.core import setup

setup(
    name='pymecab',
    version='0.0.1',
    description='Python parser for mecab output',
    author='tottoto',
    author_email='tottotodev@gmail.com',
)
