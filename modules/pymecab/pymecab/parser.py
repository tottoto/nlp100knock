from typing import List

from .morph import Morph


def _split_mecab_output_to_sentences(text: str) -> List[str]:
    return [line.strip('\n') for line in text.split('EOS') if line != '\n']


def _parse_mecab_format_word(word: str) -> Morph:
    surface, rest = word.split('\t')
    attrs = rest.split(',')
    for _ in range(9 - len(attrs)):
        attrs.append('*')
    args = {
        'surface': surface,
        'pos': attrs[0],
        'pos1': attrs[1],
        'pos2': attrs[2],
        'pos3': attrs[3],
        'ctype': attrs[4],
        'cform': attrs[5],
        'base': attrs[6],
        'reading': attrs[7],
        'pron': attrs[8],
    }
    return Morph(**args)


def _parse_mecab_format_sentence(sentence: str) -> List[Morph]:
    return [_parse_mecab_format_word(w) for w in sentence.split('\n')]


def _parse_mecab_output(s: str) -> List[List[Morph]]:
    sentences = _split_mecab_output_to_sentences(s)
    return [_parse_mecab_format_sentence(s) for s in sentences]


def parse(text: str) -> List[List[Morph]]:
    return _parse_mecab_output(text)
