from dataclasses import dataclass


@dataclass(frozen=True)
class Morph:
    surface: str
    pos: str
    pos1: str
    pos2: str
    pos3: str
    ctype: str
    cform: str
    base: str
    reading: str
    pron: str
