from knocks.knock34 import collect_connected_nouns_from_neko


class TestCollectNoConnectedNounsFromNeko:

    def test_return_appropriate_list_of_tuple_of_morphs(self):
        with open('./data/neko.txt', 'r') as f:
            neko = f.read()
        nouns = collect_connected_nouns_from_neko()
        joints = ['の', ' の', 'の ', ' の']
        for noun in nouns:
            surfaces = [noun[0].surface, noun[1].surface]
            patterns = [j.join(surfaces) for j in joints]
            assert any(pattern in neko for pattern in patterns)
