import subprocess


class TestExtractFirstColumnOfFile:

    path = './data/hightemp.txt'

    def test_return_equal_value(self):
        res1 = subprocess.run(
            ['python', './knocks/knock17.py', self.path],
            stdout=subprocess.PIPE
        ).stdout.decode()

        cut = subprocess.Popen(
            ['cut', '-f', '1', self.path],
            stdout=subprocess.PIPE
        )
        res2 = subprocess.run(
            ['sort', '-u'],
            stdin=cut.stdout,
            stdout=subprocess.PIPE,
            env={'LC_ALL': 'C'}
        ).stdout.decode()
        assert res1 == res2
