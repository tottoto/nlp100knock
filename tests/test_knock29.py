from knocks.knock29 import get_flag_url_from_england


class TestGetFlagUrlFromEngland:

    def test_return_appropriate_url_of_england_flag_image(self):
        expected = 'https://upload.wikimedia.org/wikipedia/commons/a/ae/Flag_of_the_United_Kingdom.svg'  # noqa
        actual = get_flag_url_from_england()
        assert actual == expected
