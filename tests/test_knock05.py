from knocks.knock05 import char_ngram, word_ngram


class TestCharNgram:

    SAMPLE = 'I am an NLPer'

    def test_return_appropriate_character_bi_bram_list(self):
        expected = ['I ', ' a', 'am', 'm ', ' a', 'an',
                    'n ', ' N', 'NL', 'LP', 'Pe', 'er']
        actual = char_ngram(self.SAMPLE, 2)
        assert actual == expected

    def test_return_appropriate_word_bi_bram_list(self):
        expected = ['Iam', 'aman', 'anNLPer']
        actual = word_ngram(self.SAMPLE, 2)
        assert actual == expected
