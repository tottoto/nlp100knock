import pathlib
import subprocess


class TestSplit:

    PATH = './data/hightemp.txt'

    def test_create_equal_text_files(self, tmpdir):
        prefix_1 = pathlib.Path(tmpdir.join('res1', '__'))
        subprocess.run(
            [
                'python',
                './knocks/knock16.py',
                '-n', '2',
                '-a', '3',
                self.PATH,
                str(prefix_1)
            ]
        )
        prefix_2 = pathlib.Path(tmpdir.join('res2', '__'))
        subprocess.run(
            [
                'split',
                '-n', '2',
                '-a', '3',
                self.PATH,
                str(prefix_2)
            ]
        )
        created_files_1 = list(prefix_1.glob('*'))
        created_files_2 = list(prefix_2.glob('*'))
        assert created_files_1 == created_files_2

        for p1, p2 in zip(created_files_1, created_files_2):
            with open(p1, 'r') as f:
                s1 = f.read()
            with open(p2, 'r') as f:
                s2 = f.read()
            assert s1 == s2
