from knocks.knock23 import collect_sections


class TestCollectSections:

    def test_return_appropriate_sections(self):
        expected = [(1,
                     ['国名', '歴史', '地理', '政治',
                      '外交と軍事', '地方行政区分',
                      '科学技術', '経済', '交通', '通信',
                      '国民', '文化', 'スポーツ', '脚注',
                      '関連項目', '外部リンク']),
                    (2,
                     ['気候', '主要都市', '鉱業', '農業',
                         '貿易', '通貨', '企業', '道路',
                         '鉄道', '海運', '航空', '言語',
                         '宗教', '教育', '食文化', '文学',
                         '音楽', '映画', 'コメディ', '国花',
                         '世界遺産', '祝祭日', 'サッカー', '競馬',
                         'モータースポーツ']),
                    (3, ['イギリスのポピュラー音楽'])]
        actual = collect_sections()
        assert expected == actual
