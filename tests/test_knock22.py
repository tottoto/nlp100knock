from knocks.knock22 import collect_categories


class TestCollectCategories:

    def test_return_appropriate_categories_list(self):
        expected = ['イギリス', '英連邦王国', 'G8加盟国', '欧州連合加盟国',
                    '海洋国家', '君主国', '島国', '1801年に設立された州・地域']
        actual = collect_categories()
        assert expected == actual
