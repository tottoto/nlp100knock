import pytest

from knocks.knock00 import reverse_string


class TestReverseString:

    @pytest.fixture(
        scope='function',
        params=[
            {
                'param': 'abcde',
                'expected': 'edcba'
            },
            {
                'param': 'あいうえお',
                'expected': 'おえういあ'
            },
            {
                'param': '',
                'expected': ''
            },
        ]
    )
    def valid_case(self, request):
        return request.param

    def test_return_reversed_string(self, valid_case):
        test_case = valid_case
        actual = reverse_string(test_case['param'])
        assert actual == test_case['expected']
