from knocks.knock26 import remove_emphasis_markup_from_england


class TestRemoveEmphasisMarkupFromEngland:

    def test_appropriate_str(self):
        path = './tests/data/england_removed_emphasis_markup.txt'
        with open(path, 'r') as f:
            expected = f.read()
        actual = remove_emphasis_markup_from_england()
        assert actual == expected
