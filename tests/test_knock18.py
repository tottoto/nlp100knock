import subprocess


class TestSortFile:

    path = './data/hightemp.txt'

    def test_return_equal_value(self):
        res1 = subprocess.run(
            ['python', './knocks/knock18.py', self.path],
            stdout=subprocess.PIPE
        ).stdout.decode()
        res2 = subprocess.run(
            ['sort', self.path, '-k3', '-k2', '--reverse'],
            stdout=subprocess.PIPE,
            env={'LC_ALL': 'C'}
        ).stdout.decode()
        assert res1 == res2
