from knocks.knock11 import replace_tab_to_space, replace_tab_to_space_by_tr


class TestReplaceTabToSpaceAndReplaceTabToSpaceByTr:

    def test_return_equal_value(self):
        with open('./data/hightemp.txt', 'r') as f:
            s = f.read()
        assert replace_tab_to_space(s) == replace_tab_to_space_by_tr(s)
