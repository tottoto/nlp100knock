import json

from knocks.knock25 import parse_basic_information_in_england


class TestParseBasicInformationInEngland:

    def test_return_appropriate_dict(self):
        with open('./tests/data/basic_information_of_england.json', 'r') as f:  # noqa
            expected = json.load(f)
        actual = parse_basic_information_in_england()
        for key in actual.keys():
            assert actual[key] == expected[key]
