import json

from knocks.knock32 import extract_base_of_verb_from_neko


class TestExtractBaseOfVerbFromNeko:

    def test_return_appropriate_set_of_base_of_verb(self):
        actual = extract_base_of_verb_from_neko()
        assert all(map(lambda x: isinstance(x, str), actual))
        with open('./tests/data/neko_base_of_verb.json', 'r') as f:
            expected = set(json.load(f))
        assert actual == expected
