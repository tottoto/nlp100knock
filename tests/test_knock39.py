from matplotlib.pyplot import Figure

from knocks.knock39 import plot_zipf_of_neko


class TestPlotZipfOfNeko:

    def test_return_appropriate_pyplot_figure_instance(self):
        fig = plot_zipf_of_neko()
        assert isinstance(fig, Figure)
