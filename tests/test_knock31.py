import json

from knocks.knock31 import extract_surface_of_verb_from_neko


class TestExtractSurfaceOfVerbFromNeko:

    def test_return_appropriate_list_of_surface_of_verb(self):
        with open('./tests/data/neko_surface_of_verb.json', 'r') as f:
            expected = json.load(f)
        actual = extract_surface_of_verb_from_neko()
        assert actual == expected
