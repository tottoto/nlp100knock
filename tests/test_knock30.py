from knocks.knock30 import parse_neko_mecab


class TestParseNekoMecab:

    def test_return_appropriate_list_of_list_of_morph(self):
        expected = {
            'surface': '一',
            'pos': '名詞',
            'pos1': '数',
            'pos2': '*',
            'pos3': '*',
            'ctype': '*',
            'cform': '*',
            'base': '一',
            'reading': 'イチ',
            'pron': 'イチ'
        }
        actual = parse_neko_mecab()
        for key, expected_value in expected.items():
            assert getattr(actual[0][0], key) == expected_value
