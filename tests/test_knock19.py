import subprocess


class TestCollectFirstColumnAndSortByCounts:

    path = './data/hightemp.txt'

    def test_return_equal_value(self):
        res1 = subprocess.run(
            ['python', './knocks/knock19.py', self.path],
            stdout=subprocess.PIPE
        ).stdout.decode()
        # cut -f 1 data/hightemp.txt | sort | uniq -c
        # | sed "s/^ \{1,\}//"| LC_ALL=C sort -k1 -k2 --reverse
        # | cut -d ' ' -f 2
        cut_1 = subprocess.Popen(
            ['cut', '-f', '1', self.path],
            stdout=subprocess.PIPE
        )
        sort_1 = subprocess.Popen(
            'sort',
            stdin=cut_1.stdout,
            stdout=subprocess.PIPE
        )
        uniq = subprocess.Popen(
            ['uniq', '-c'],
            stdin=sort_1.stdout,
            stdout=subprocess.PIPE
        )
        sed = subprocess.Popen(
            ['sed', r's/^ \{1,\}//'],
            stdin=uniq.stdout,
            stdout=subprocess.PIPE
        )
        sort_2 = subprocess.Popen(
            ['sort', '-k1', '-k2', '--reverse'],
            stdin=sed.stdout,
            stdout=subprocess.PIPE,
            env={'LC_ALL': 'C'}
        )
        res2 = subprocess.run(
            ['cut', '-d', ' ', '-f', '2'],
            stdin=sort_2.stdout,
            stdout=subprocess.PIPE
        ).stdout.decode()
        assert res1 == res2
