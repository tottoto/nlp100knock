from knocks.knock06 import SetCalculator


class TestSetCalculator:

    TEXT_1 = 'paraparaparadise'
    TEXT_2 = 'paragraph'
    set_calculator = SetCalculator(TEXT_1, TEXT_2)

    def test_return_appropriate_unioned_set(self):
        expected = {'a', 'd', 'e', 'g', 'h', 'i', 'p', 'r', 's'}
        actual = self.set_calculator.union()
        assert actual == expected

    def test_return_appropriate_difference_set(self):
        expected = {'d', 'e', 'i', 's'}
        actual = self.set_calculator.difference()
        assert actual == expected

    def test_return_appropriate_intersection_set(self):
        expected = {'a', 'p', 'r'}
        actual = self.set_calculator.intersection()
        assert actual == expected
