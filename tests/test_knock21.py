from knocks.knock21 import collect_category_lines


class TestCollectCategoryLines:

    def test_return_list_of_line_containing_category(self):
        with open('./tests/data/england_category_lines.txt', 'r') as f:
            category_lines_txt = f.read()
        expected = category_lines_txt.split()
        actual = collect_category_lines()
        assert actual == expected
