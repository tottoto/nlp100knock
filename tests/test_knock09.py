from knocks.knock09 import random, typoglycemia


class TestTypoglycemia:

    random.seed(0)

    TEXT = ('I couldn\'t believe that I could actually understand what '
            'I was reading : the phenomenal power of the human mind .')

    def test_appropriate_typoglycemia(self):
        expected = (
            'I cd\'oulnt bieleve that I cuold alutacly utrsnedand what '
            'I was rniadeg : the phaeonneml power of the huamn mind .'
        )
        actual = typoglycemia(self.TEXT)
        assert actual == expected
