from knocks.knock33 import extract_morph_of_sahen_connection_noun_from_neko
from pymecab.morph import Morph


class TestExtractMorphOfSahenConnectionNounFromNeko:

    def test_return_list_of_morph_that_is_sahen_connection_noun(self):
        actual = extract_morph_of_sahen_connection_noun_from_neko()
        for w in actual:
            assert isinstance(w, Morph)
            assert w.pos == '名詞'
            assert 'サ変接続' in [w.pos1, w.pos2, w.pos2]
