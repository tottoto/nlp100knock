from knocks.knock35 import collect_nouns_sequences_from_neko


class TestCollectNounsSequenceFromNeko:

    def test_return_appropriate_nouns_sequences_from_neko(self):
        sequences = collect_nouns_sequences_from_neko()
        for seq in sequences:
            assert len(seq) >= 2
            for w in seq:
                assert w.pos == '名詞'
