from knocks.knock08 import cipher


class TestCipher:

    def test_return_encrypted_string(self):
        expected = '!zyx.あ'
        actual = cipher('!abc.あ')
        assert actual == expected

    def test_return_decrypted_string(self):
        arg = '!abcde.あ'
        assert arg == cipher(cipher(arg))
