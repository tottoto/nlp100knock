from knocks.knock15 import tail, tail_by_command


class TestTail:

    def test_return_equal_value(self):
        path = './data/hightemp.txt'
        number = 3
        res1 = tail(path, number)
        res2 = tail_by_command(path, number)
        assert res1 == res2
