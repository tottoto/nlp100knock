from knocks.knock36 import count_morphs_frequency_of_appearance_in_neko
from pymecab.morph import Morph


class TestCountMorphsFrequencyOfAppearanceInNeko:

    def test_return_appropriate_dict(self):
        actual = count_morphs_frequency_of_appearance_in_neko()
        pre_value = 1
        s = 0
        for key, value in actual.items():
            s += value
            assert isinstance(key, Morph)
            assert pre_value >= value
            pre_value = value
        assert 1 - s <= 1e-6
