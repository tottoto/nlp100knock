from knocks.knock03 import get_pi


class TestGetPi:

    def test_return_pi(self):
        expected = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9]
        actual = get_pi()
        assert actual == expected
