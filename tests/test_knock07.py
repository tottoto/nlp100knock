from knocks.knock07 import get_string


class TestGetString:

    def test_return_appropriate_string(self):
        expected = '12時の気温は22.4'
        actual = get_string(12, '気温', 22.4)
        assert actual == expected
