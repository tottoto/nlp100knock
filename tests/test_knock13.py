from knocks.knock13 import merge_cols_as_tsv, merge_cols_as_tsv_by_paste


class TestMergeColsAsTsv:

    def test_return_equal_value(self):
        paths = ['./data/col1.txt', './data/col2.txt']
        res1 = merge_cols_as_tsv(paths)
        res2 = merge_cols_as_tsv_by_paste(paths)
        assert res1 == res2
