from knocks.knock12 import get_col_by_cut, save_col1_and_col2_of_hightemp


class TestSaveCol1AndCol2OfHightempAndGetColByCut:

    def test_return_equal_value(self, tmpdir):
        path1 = str(tmpdir.join('col1.txt'))
        path2 = str(tmpdir.join('col2.txt'))
        save_col1_and_col2_of_hightemp(
            col1_path=path1,
            col2_path=path2
        )
        with open(path1, 'r') as f:
            saved_col1 = f.read()
        with open(path2, 'r') as f:
            saved_col2 = f.read()
        with open('./data/hightemp.txt', 'r') as f:
            hightemp = f.read()
        assert saved_col1 == get_col_by_cut(hightemp, 1)
        assert saved_col2 == get_col_by_cut(hightemp, 2)
