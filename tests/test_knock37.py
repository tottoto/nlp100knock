import operator

from knocks.knock37 import get_top_10_words_of_neko, plot_top_10_words_of_neko


class TestGetTop10WordsOfNeko:

    def test_return_appropriate_list_of_tupple(self):
        actual = get_top_10_words_of_neko()
        assert len(actual) == 10
        assert isinstance(actual, dict)
        values = list(actual.values())
        assert all(map(
            lambda x: operator.ge(*x),
            zip(values, values[1:]))
        )


class TestPlotTop10WordsOfNeko:

    def test_does_not_raise_any_exception(self):
        plot_top_10_words_of_neko()
