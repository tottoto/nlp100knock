from knocks.knock14 import head, head_by_command


class TestHead:

    def test_return_equal_value(self):
        path = './data/hightemp.txt'
        number = 3
        res1 = head(path, number)
        res2 = head_by_command(path, number)
        assert res1 == res2
