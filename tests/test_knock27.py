import os

from knocks.knock27 import \
    remove_emphasis_and_internal_link_markup_from_england


class TestRemoveEmphasisAndInternalLinkMarkupFromEngland:

    def test_return_appropriate_string(self):
        path = os.path.join(
            './',
            'tests',
            'data',
            'england_removed_emphasis_and_internal_link_markup.txt'
        )
        with open(path, 'r') as f:
            expected = f.read()
        actual = remove_emphasis_and_internal_link_markup_from_england()
        assert actual == expected
