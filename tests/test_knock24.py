from knocks.knock24 import collect_files


class TestCollectFiles:

    def test_return_appropriate_filenames_list(self):
        expected = ['Battle of Waterloo 1815.PNG',
                    'The British Empire.png',
                    'Uk topo en.jpg',
                    'BenNevis2005.jpg',
                    'Elizabeth II greets NASA GSFC employees, May 8, 2007 edit.jpg',  # noqa
                    'Palace of Westminster, London - Feb 2007.jpg',
                    'David Cameron and Barack Obama at the G20 Summit in Toronto.jpg',  # noqa
                    'Soldiers Trooping the Colour, 16th June 2007.jpg',
                    'Scotland Parliament Holyrood.jpg',
                    'London.bankofengland.arp.jpg',
                    'City of London skyline from London City Hall - Oct 2008.jpg',  # noqa
                    'Oil platform in the North SeaPros.jpg',
                    'Eurostar at St Pancras Jan 2008.jpg',
                    'Heathrow T5.jpg',
                    'Anglospeak.svg']
        actual = collect_files()
        assert actual == expected
