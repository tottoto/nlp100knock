from knocks.knock10 import count_file_lines, count_file_lines_by_wc


class TestCountFileLinesAndCountFileLinesByWc:

    def test_return_equal_value(self):
        path = './data/hightemp.txt'
        assert count_file_lines(path) == count_file_lines_by_wc(path)
