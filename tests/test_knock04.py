from knocks.knock04 import get_elements


class TestsGetElements:

    def test_return_elements_dict(self):
        expected = {1: 'H',
                    2: 'He',
                    3: 'Li',
                    4: 'Be',
                    5: 'B',
                    6: 'C',
                    7: 'N',
                    8: 'O',
                    9: 'F',
                    10: 'Ne',
                    11: 'Na',
                    12: 'Mi',
                    13: 'Al',
                    14: 'Si',
                    15: 'P',
                    16: 'S',
                    17: 'Cl',
                    18: 'Ar',
                    19: 'K',
                    20: 'Ca'}
        actual = get_elements()
        assert actual == expected
