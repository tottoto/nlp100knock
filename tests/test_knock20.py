import json

from knocks.knock20 import load_england_record


class TestLoadEnglandRecord:

    england = json.load(open('./tests/data/england.json', 'r'))

    def test_return_england_dict(self):
        expected = self.england
        actual = load_england_record('./data/jawiki-country.json')
        assert actual == expected
