FROM registry.gitlab.com/tottoto/docker-python-mecab:0.1.0
LABEL maintainer="tottoto <tottotodev@gmail.com>"

WORKDIR /nlp100knock
ENV PYTHONPATH .
ADD Pipfile Pipfile
ADD Pipfile.lock Pipfile.lock
RUN pipenv install --ignore-pipfile --dev --skip-lock
